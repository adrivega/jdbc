package controlador;

import modelo.*;

import java.sql.*;
import java.util.ArrayList;

public class ProductosJDBC implements ProductosDAO {

    public final String SELECT_ALL = "SELECT * FROM productos";
    public final String SELECT_BY_ID = "SELECT * FROM productos WHERE id = ?";
    public final String INSERT_PRODUCTO = "INSERT productos VALUES(?, ?, ?)";
    public final String DELETE_PRODUCTOS_BY_ID = "DELETE FROM productos WHERE id = ?";
    public final String UPDATE_ALL_BY_ID_PRODUCTOS = "UPDATE productos SET id = ?, nombre = ?, precio = ?, WHERE id_jugador = ?";


    Connection con;

    public ProductosJDBC(Connection con){
        this.con=con;
    }

    @Override
    public ArrayList<Productos> select_all() {
        ArrayList<Productos> lista_productos = new ArrayList<Productos>();

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                Double precio = rs.getDouble("precio");

                Productos p1 = new Productos(id, nombre, precio);
                lista_productos.add(p1);

            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_productos;
    }

    @Override
    public Productos select_by_id(int id) {

        Productos p1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int id2 = rs.getInt("id");
                String name = rs.getString("name");
                Double precio = rs.getDouble("precio");

                p1 = new Productos(id2, name,precio);

            }else{
                p1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return p1;
    }

    @Override
    public boolean insert_productos(Productos productos) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_PRODUCTO);
            stmt.setInt(1, productos.getId());
            stmt.setString(2,productos.getNombre());
            stmt.setDouble(3,productos.getPrecio());

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean delete_clientes_by_id(int id) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_PRODUCTOS_BY_ID);
            stmt.setInt(1, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean update_all_by_id_productos(Productos productos){
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ALL_BY_ID_PRODUCTOS);
            stmt.setInt(1, productos.getId());
            stmt.setString(2,productos.getNombre());
            stmt.setDouble(3,productos.getPrecio());
            int x = stmt.executeUpdate();
            if(x!=1){
                salida = false;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

}