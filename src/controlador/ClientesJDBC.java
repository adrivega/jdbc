package controlador;

import modelo.ClientesDAO;
import modelo.Clientes;

import java.sql.*;
import java.util.ArrayList;

public class ClientesJDBC implements ClientesDAO {

    public final String SELECT_ALL = "SELECT * FROM clientes";
    public final String SELECT_BY_ID = "SELECT * FROM clientes WHERE id = ?";
    public final String INSERT_CLIENTES = "INSERT clientes VALUES(?, ?, ?, ?, ?, ?)";
    public final String DELETE_CLIENTES_BY_ID = "DELETE FROM clientes WHERE id = ?";
    public final String UPDATE_ALL_BY_ID_CLIENTE = "UPDATE clientes SET id = ?, nombre = ?, apellido = ?, email = ?, telefono = ?, direccion = ?, WHERE id_jugador = ?";


    Connection con;

    public ClientesJDBC(Connection con){
        this.con=con;
    }

    @Override
    public ArrayList<Clientes> select_all() {
        ArrayList<Clientes> lista_clientes = new ArrayList<Clientes>();

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                String nombre = rs.getString("nombre");
                int id = rs.getInt("id");
                String email = rs.getString("email");
                String apellido = rs.getString("apellido");
                int telefono = rs.getInt("telefono");
                String direccion = rs.getString("direccion");

                Clientes c1 = new Clientes(id,nombre, apellido, email, telefono, direccion);
                lista_clientes.add(c1);

            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_clientes;
    }

    @Override
    public Clientes select_by_id(int id) {

        Clientes c1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                String nombre = rs.getString("nombre");
                int id2 = rs.getInt("id");
                String email = rs.getString("email");
                String apellido = rs.getString("apellido");
                int telefono = rs.getInt("telefono");
                String direccion = rs.getString("direccion");

                c1 = new Clientes(id2, nombre, email, apellido, telefono, direccion);

            }else{
                c1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return c1;
    }

    @Override
    public boolean insert_clientes(Clientes clientes) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_CLIENTES);
            stmt.setInt(1, clientes.getId());
            stmt.setString(2, clientes.getName());
            stmt.setString(3,clientes.getApellido());
            stmt.setString(4, clientes.getEmail());
            stmt.setInt(5,clientes.getTelefono());
            stmt.setString(6,clientes.getDireccion());

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }


    @Override
    public boolean delete_clientes_by_id(int id) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_CLIENTES_BY_ID);
            stmt.setInt(1, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean update_all_by_id_Cliente(Clientes clientes){
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ALL_BY_ID_CLIENTE);
            stmt.setInt(1, clientes.getId());
            stmt.setString(2, clientes.getName());
            stmt.setString(3,clientes.getApellido());
            stmt.setString(4, clientes.getEmail());
            stmt.setInt(5,clientes.getTelefono());
            stmt.setString(6,clientes.getDireccion());
            int x = stmt.executeUpdate();
            if(x!=1){
                salida = false;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

}
