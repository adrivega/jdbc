package controlador;

import modelo.*;

import java.sql.*;
import java.util.ArrayList;

public class PedidosJDBC implements PedidosDAO {

    public final String SELECT_ALL = "SELECT * FROM pedidos";
    public final String SELECT_BY_ID = "SELECT * FROM pedidos WHERE id = ?";
    public final String INSERT_PEDIDOS = "INSERT pedidos VALUES(?, ?, ?)";
    public final String DELETE_PEDIDOS_BY_ID = "DELETE FROM pedidos WHERE id = ?";
    public final String UPDATE_ALL_BY_ID_PEDIDOS = "UPDATE pedidos SET id = ?, clienteID = ?, productoID = ?, email = ?, cantidad = ?, direccion = ?, WHERE id_jugador = ?";

    Connection con;

    public PedidosJDBC(Connection con){
        this.con=con;
    }

    @Override
    public ArrayList<Pedidos> select_all() {
        ArrayList<Pedidos> lista_pedidos = new ArrayList<Pedidos>();

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                int clienteID = rs.getInt("clienteID");
                int productoID = rs.getInt("pruductoID");
                int cantidad = rs.getInt("cantidad");
                String direccion = rs.getString("direccion");

                Pedidos p1 = new Pedidos(id, clienteID, productoID, cantidad, direccion);
                lista_pedidos.add(p1);

            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_pedidos;
    }

    @Override
    public Pedidos select_by_id(int id) {

        Pedidos p1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int id3 = rs.getInt("id");
                int clienteID = rs.getInt("clienteID");
                int productoID = rs.getInt("pruductoID");
                int cantidad = rs.getInt("cantidad");
                String direccion = rs.getString("direccion");

                p1 = new Pedidos(id3, clienteID, productoID, cantidad, direccion);

            }else{
                p1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return p1;
    }


    @Override
    public boolean insert_pedidos(Pedidos pedidos) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_PEDIDOS);
            stmt.setInt(1, pedidos.getId());
            stmt.setInt(2,pedidos.getClienteID());
            stmt.setInt(3,pedidos.getProductoID());
            stmt.setInt(4, pedidos.getCantidad());
            stmt.setString(5, pedidos.getDireccion());

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean delete_clientes_by_id(int id) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_PEDIDOS_BY_ID);
            stmt.setInt(1, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean update_all_by_id_pedidos(Pedidos pedidos){
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ALL_BY_ID_PEDIDOS);
            stmt.setInt(1, pedidos.getId());
            stmt.setInt(2,pedidos.getClienteID());
            stmt.setInt(3,pedidos.getProductoID());
            stmt.setInt(4, pedidos.getCantidad());
            stmt.setString(5, pedidos.getDireccion());
            int x = stmt.executeUpdate();
            if(x!=1){
                salida = false;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

}