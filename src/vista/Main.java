package vista;

import Utilidades.Conexion;
import controlador.ClientesJDBC;
import controlador.PedidosJDBC;
import controlador.ProductosJDBC;
import modelo.Clientes;
import modelo.Pedidos;
import modelo.Productos;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Conexion c = new Conexion();
        Connection conn = null;
        PreparedStatement stmt = null;

        Conexion c1 = new Conexion();
        Connection conn1 = null;
        PreparedStatement stmt1 = null;

        Conexion c2 = new Conexion();
        Connection conn2 = null;
        PreparedStatement stmt2 = null;

        try{

            System.out.println("Connecting to database...");
            conn = c.getConnection();
            ClientesJDBC controller = new ClientesJDBC(conn);
            ArrayList<Clientes> lista_clientes = controller.select_all();
            for (Clientes j: lista_clientes) {
                System.out.println(j);
            }

            conn1 = c1.getConnection();
            ProductosJDBC controller1 = new ProductosJDBC(conn1);
            ArrayList<Productos> lista_productos = controller1.select_all();
            for (Productos e: lista_productos) {
                System.out.println(e);
            }
            /*
            conn2 = c2.getConnection();
            PedidosJDBC controller2 = new PedidosJDBC(conn2);
            ArrayList<Pedidos> lista_pedidos = controller2.select_all();
            for (Pedidos p: lista_pedidos) {
                System.out.println(p);
            } NO SE PORQUE SALTA UN ERROR AL EJECUTAR Y PONE QUE NO ESNCUENTRA LA COLUMNA productoID*/


        } catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }//end main
}