package modelo;

import java.util.ArrayList;

public interface ClientesDAO {
    ArrayList<Clientes> select_all();
    Clientes select_by_id(int id);
    boolean insert_clientes(Clientes clientes);

    boolean delete_clientes_by_id(int id);

    boolean update_all_by_id_Cliente(Clientes clientes);
}
