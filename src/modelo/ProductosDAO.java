package modelo;

import java.util.ArrayList;

public interface ProductosDAO {
    ArrayList<Productos> select_all();
    Productos select_by_id(int id);
    boolean insert_productos(Productos productos);
    boolean delete_clientes_by_id(int id);
    boolean update_all_by_id_productos(Productos productos);
}
