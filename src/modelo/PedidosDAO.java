package modelo;

import java.util.ArrayList;

public interface PedidosDAO {
    ArrayList<Pedidos> select_all();
    Pedidos select_by_id(int id);
    boolean insert_pedidos(Pedidos pedidos);

    boolean delete_clientes_by_id(int id);

    boolean update_all_by_id_pedidos(Pedidos pedidos);
}