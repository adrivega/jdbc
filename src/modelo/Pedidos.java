package modelo;

public class Pedidos {
    private int id;
    private int clienteID;
    private int productoID;
    private int cantidad;
    private String direccion;

    public Pedidos(int id, int clienteID, int productoID, int cantidad, String direccion) {
        this.id = id;
        this.clienteID = clienteID;
        this.productoID = productoID;
        this.cantidad = cantidad;
        this.direccion = direccion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClienteID() {
        return clienteID;
    }

    public void setClienteID(int clienteID) {
        this.clienteID = clienteID;
    }

    public int getProductoID() {
        return productoID;
    }

    public void setProductoID(int productosID) {
        this.productoID = productosID;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Pedidos{" +
                "id=" + id +
                ", clienteID=" + clienteID +
                ", productoID=" + productoID +
                ", cantidad=" + cantidad +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
