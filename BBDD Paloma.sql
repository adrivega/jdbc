create database Ejercicio;
use Ejercicio;

create table clientes(
	id int primary key,
    nombre varchar(50),
    apellido varchar(50),
    email varchar(100),
    telefono int (9),
    direccion varchar(100)
	);
create table productos(
	id int primary key,
    nombre varchar (100),
    precio decimal (10,2)
);
create table pedidos(
	id int primary key,
    clienteID int,
    productoID int,
    cantidad int,
    FechaPedido date
);

insert clientes values (1, "Carlos", "Nevado", "carlosnevado@gmail.com",123456789,"Madrid");
insert clientes values (2, "Hugo", "Bravo", "hugobravo@gmail.com",112345678,"Toledo");
insert clientes values (3, "Victor", "Garcia", "victorgarcia@gmail.com",122345678,"Segovia");
insert clientes values (4, "Daniel", "Navas", "danielnavas@gmail.com",123345678,"Extremadura");
insert clientes values (5, "Alejandro", "Redomero", "alejandroredomero@gmail.com",123445678,"Sevilla");

insert productos values (22345, "Camiseta", "15.50");
insert productos values (11234, "Pantalon", "20.99");
insert productos values (12234, "Zapatos", "45.55");
insert productos values (12334, "Sudadera", "35.15");
insert productos values (12344, "Gorra", "09.99");

insert pedidos values (01 , 1 , 12345 , 2 ,null, Madrid);
insert pedidos values (02 , 2 , 11234 , 1 ,null, Barcelona);
insert pedidos values (03 , 3 , 12234 , 3 ,null, Sevilla);


use Ejercicio;
alter table pedidos ADD column direccion varchar(50);